Recipe book made recipes defined in the [Open Recipe Format](https://open-recipe-format.readthedocs.io/en/latest/)

The deployed version can be found at https://recipes.jeroenpelgrims.com/
