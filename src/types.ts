import * as z from "zod";

const AmountSchema = z.object({
  amount: z.number(),
  unit: z.string(),
});

const StepSchema = z.object({
  step: z.string(),
  haccp: z
    .object({
      control_point: z.string().optional(),
      critical_control_point: z.string().optional(),
    })
    .optional(),
  notes: z.array(z.string()).optional(),
});

const YieldSchema = z.record(z.number());

// separately defined to support recursiveness
// https://github.com/bmcniel/zod#recursive-types
export type Ingredient = {
  [ingredientName: string]: {
    usda_num?: string;
    processing?: string[];
    amounts?: Amount[];
    notes?: string[];
    substitutions?: Ingredient[];
  } | null;
};

const IngredientSchema: z.ZodType<Ingredient> = z.lazy(() => {
  return z.record(
    z
      .object({
        usda_num: z.string().optional(),
        processing: z.array(z.string()).optional(),
        amounts: z.array(AmountSchema),
        notes: z.array(z.string()).optional(),
        substitutions: z.array(IngredientSchema).optional(),
      })
      .nullable()
  );
});

const RecipeSchema = z.object({
  recipe_name: z.string(),
  source_url: z.string().optional(),
  ingredients: z.array(IngredientSchema),
  steps: z.array(StepSchema),
  yields: z.array(YieldSchema).optional(),
});

export type Amount = z.infer<typeof AmountSchema>;
export type Step = z.infer<typeof StepSchema>;
export type Yield = z.infer<typeof YieldSchema>;
export type Recipe = z.infer<typeof RecipeSchema>;

export const schemas = {
  AmountSchema,
  StepSchema,
  YieldSchema,
  IngredientSchema,
  RecipeSchema,
};
