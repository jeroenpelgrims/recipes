import recipes, { getRecipeUrl } from "./data/recipes";
import { createPage, copyStatic, rootPath } from "./lib";
import { join, resolve, relative } from "path";

const recipeArray = recipes();

function createRecipePages() {
  for (const { slug, recipe, localPath } of recipeArray) {
    createPage({
      path: getRecipeUrl(slug),
      data: {
        recipe,
        yields: recipe.yields || undefined,
      },
      template: "templates/recipe.ejs",
    });
    copyStatic(localPath, join(rootPath, getRecipeUrl(slug), "recipe.yml"));
  }
}

function createIndexPage() {
  createPage({
    path: "/",
    template: "templates/index.ejs",
    data: {
      recipes: recipeArray,
      getRecipeUrl,
    },
  });
}

function createAboutPage() {
  createPage({
    path: "/about",
    template: "templates/about.ejs",
    data: {},
  });
}

createIndexPage();
createAboutPage();
createRecipePages();

copyStatic(join("src", "static", "style.css"));
copyStatic(join("src", "static", "scripts.js"));
