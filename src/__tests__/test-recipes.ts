import { recipePaths, readRecipe } from '../data/recipes';

describe("Recipes have correct format", () => {
  for (let path of recipePaths) {
    it(path, () => {
      expect(() => readRecipe(path)).not.toThrow();
    });
  }
});