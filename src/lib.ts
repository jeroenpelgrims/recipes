import { writeFileSync, mkdirSync, readFileSync } from "fs";
import { join, resolve, basename } from "path";
import ejs from "ejs";
import { copyFileSync } from "fs";

export const rootPath = resolve("./public");

type PageOptions<TData = object> = {
  path: string;
  template: string;
  data?: TData;
};

export function createPage(options: PageOptions) {
  const pagePath = join(rootPath, options.path);
  const indexFile = join(pagePath, "index.html");
  const templateFn = compileTemplate(join("src", options.template));
  const html = templateFn(options.data);

  mkdirSync(pagePath, { recursive: true });
  writeFileSync(indexFile, html);
}

function compileTemplate(templatePath: string) {
  const template = readFileSync(templatePath).toString();
  let compiled = ejs.compile(template, {
    root: join(__dirname, "templates"),
  });
  return compiled;
}

export function copyStatic(from: string, to?: string) {
  to = to === undefined ? join(rootPath, basename(from)) : to;
  copyFileSync(from, to);
}
