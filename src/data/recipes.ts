import { join } from "path";
import walkSync from "walk-sync";
import { readFileSync } from "fs";
import yaml from "js-yaml";
import { schemas, Recipe } from "../types";
import { dirname, basename } from "path";

const recipeFolder = join(__dirname, "..", "..", "recipes");

export const recipePaths = walkSync(recipeFolder, {
  directories: false,
  globs: ["**/**.yml"],
}).map(path => join(recipeFolder, path));

export function readRecipe(recipePath: string) {
  const recipe = readFileSync(recipePath).toString();
  const data = yaml.load(recipe) as Recipe;
  const result = schemas.RecipeSchema.parse(data);
  return result as Recipe;
}

export function getRecipeSlug(recipePath: string) {
  return basename(dirname(recipePath));
}

export type RecipeData = {
  localPath: string;
  slug: string;
  recipe: Recipe;
};

export default () => {
  return recipePaths.reduce((result, recipePath) => {
    result.push({
      localPath: recipePath,
      slug: getRecipeSlug(recipePath),
      recipe: readRecipe(recipePath),
    });

    return result;
  }, [] as RecipeData[]);
};

export function getRecipeUrl(slug: string) {
  return "/recipes/" + slug;
}
